# Music Genre Recognition
This project is a required project for course Scientific Computing. It's an elective on masters programme on Belgrade
University, School of Mathematics. Topic I've chosen is music genre recognition (MGR). MGR is a problem of identifying
genre for a given audio sample. Input to MGR is raw audio signal, while output is a probability distribution over
predefined set of genres. Music Information Retrieval (MIR) as a field of applied machine learning where MGR is only a
part of. So to speak, music genre recognition is to music information retrieval as image classification is to computer
vision.

Table of contents:
1. [Dataset Description](#1-dataset-description)
1. [Approaches](#2-approaches)

## 1. Dataset Description
For this project, I decided to use [Free Music Archive](https://arxiv.org/pdf/1612.01840.pdf) (FMA) dataset, as it is
rich in the number of tracks, artists, styles, and sound quality. FMA dataset contains a bit more than 100K. Genres are
defined by a genre tree. For example genre *Deep Funk* is a sub-genre of *Funk* which in turn is a sub-genre of *Soul
R&B*. Genre tree contains 16 top-level nodes, i.e. 16 genres that are considered to be *base genres*. Whole FMA genre
tree contains 163 nodes, which means that there's total of 163 genres. However they are not completely disjoint because,
as previously stated, some of them are just parent-genres of others. Because of this and the fact that leaf-genres (124 
of them) are poorly balanced, I've decided to state my MGR problem only in terms of base genres, so classification is
going to be performed over 16 classes.

## 2. Approaches
Currently there are two dominant ways present in the community for solving MGR. One is using precomputed set of features
(e.g. amplitude of frequencies, Mel-frequency cepstrum, Hamming distances of windows etc.) to feed into traditional
machine learning classifier such as SVMs or RDFs. Second approach is based on deep learning. Here, I'm going to focus on
deep learning based approaches and I'll try to reproduce results from selected papers, and provide a couple of novel
approaches for MGR. One way to think about audio signal classification is to reduce it to a computer vision problem by
representing an audio signal with it's spectrogram upon which one is able to use the whole arsenal of CNNs like VGG,
Resnet, InceptionNet etc. Another deep learning approach is to use sequence models, e.g. LSTM or other RNN variations.
I've failed to find any research paper that uses plain strided 1D convolution. In field of video recognition where
sequence analysis is important, temporal convolutional networks gave significant improvements, so it's natural idea to
try and use them in the field of MGR.